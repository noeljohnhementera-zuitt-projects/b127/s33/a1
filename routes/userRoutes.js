
// Activity at Line 33

const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers")
// We need to require express in order to use the router

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res)=>{
	userControllers.checkEmailExists(req.body)
	.then(result => {
		res.send(result);
	});
})

// Route for user registration
router.post("/register", (req, res)=>{
	userControllers.registerUser(req.body)
	.then(result => {
		res.send(result)
	})
})

// Route for User Authentication
router.post("/login", (req, res)=>{
	userControllers.loginUser(req.body)
	.then(result => {
		res.send(result)
	})
})

// Start of Activity
router.get("/allUsers", (req, res) =>{
	userControllers.allUsers()
	.then(users =>{
		res.send(users)
	})
})

router.post("/details", (req, res)=>{
	userControllers.getProfile(req.body)
	.then(userId => {
		res.send(userId)
	})
})
module.exports = router;