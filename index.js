const express = require("express");
const app = express();
const mongoose = require("mongoose");

const cors = require("cors");
// A browser mechanism which enables controlled access to resources located outside of a given domain
// Allows our back-end application to be available to our front-end application
// Allows us to control app's Cross Origin Resource Sharing (cors) Settings
	// to set the allowed endpoints or urls
	// If what sites ang allowed to enter sa server natin

// Allow access to routes defined within our application
const userRoutes = require("./routes/userRoutes");

// Connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.z9use.mongodb.net/Batch127_Booking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"));
// Prompts a message in the terminal once the connection is "open" and we are able to successfully connect to our database

app.use(cors());
// Allows all resources/origins to access our backend application
// Checks kung sino ang allowed origins ang allowed sa server
	//  put URL if may irerestrict
// Whitelist = are allowed url or origins or endpoints

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Create a base URL and connected route that will apply the base url
app.use("/users", userRoutes);
// Base URL, "http://localhost:4000/users"

app.listen(process.env.PORT || 4000, () =>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})
// Will used the defined port number for the application whenever an environment variable is available 
	// OR will used port 4000 if none is defined
		// Since we are local, 4000 ang gagamitin
// The syntax, "process.env.PORT || 4000" will allow flexibility when using the application locally or as a hosted application
// Allows the port to be flexible locally or hosted application