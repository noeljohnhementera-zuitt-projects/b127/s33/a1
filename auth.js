// Dito isasave ang json web token

// JSON web token or JWT is a way of securely passing information from the server to the 
	// frontend or to other part of server
// Information is kept secure through the use of the secret code
	// only the system knows the secret code that can decode the encrypted information
	// secret is equal to a lock code

const jwt = require("jsonwebtoken");
// to create a payload in order to pass data sa users

// User defined string data that will be used to create our JSON web tokens
// used in the algorithm for encrypting our data which makes it difficult to decode the
	// information w/out the defined secret keyword

const secret = "CourseBookingAPI";
// Encryption in our ticket
// para secure ang info
	// server lang ang may alam ng secret

// Token Creation
// Analogy = pack the gift and provide a lock with the secret code as the key
module.exports.createAccessToken = (user) =>{
	// The data will be received from the registration form
		// so when the user logs in, a token will be created with users information
	const data = {
		// data na manggagaling sa registered user
		id: user._id, // property from stored user
		email: user.email,
		isAdmin: user.isAdmin
	};

	// Generate a JSON Web Token using the jwt's sign method(signature)
	// Generates the token using the form data and the secret code with no addition options provided
	return jwt.sign(data, secret, {})
}

 