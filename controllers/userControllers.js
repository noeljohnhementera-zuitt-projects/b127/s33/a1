
// Activity at Line 104

const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if the email already exist
/*
Business model
1. Use mongoose "find" method to find duplicate emails
2. Use the .then method to send a response back to the client 
based on the result of the find method (email already exists / does not exist) 
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find( {email: reqBody.email} )
	.then(result =>{
		//The "find" method returns a record if a match is found
		if(result.length > 0){
			return true;
		}else{
			// No duplicate email found
			// The user is not yet registered in the database
			return false;
		}
	})
}	

//User registration
/*
Business Logic
1. Create a new user object using the mongoose model and the info from the request body
2. Error handling
	= if error, return error
	= else, save the new User to the database
*/

// This uses the information from the request body to provide all the necessary information
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,	// galing kay client
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run
			// in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password, 10)
	})			  // this is used to hash or encrypt a password
	// Saves the created object to our database
	return newUser.save()
	.then((user, error)=>{
		// if user reg fails
		if(error){
			return false;
		}else{
			// User reg is successful
			return true;
		}
	})
}

// User authentication
/*
Business Logic
1. Check the database if the user email exists
2. Compare the password provided in the login form with the password stored in the database
3. Generate or return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) =>{
	return User.findOne( {email: reqBody.email})
	.then(result =>{
		// If user does not exist
		if(result === null){
			return false;
		}else{
			//User exists
			// Create a variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// "compareSync" method is used to compare a non encrypted password from the login form
				// to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			
			// A good practice for boolean variable or constants is to use the word "is" or "are" 
				// at the beginning in the form of is+Noun
					// eg. isDone, isAdmin, areFinish, etc 
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			// If the password match/result o the above code is true
			if(isPasswordCorrect){
				// Generate an access token
				// use the "createAccessToken" method (dahil gumamit na tayo ng module) defined in the "auth.js" file
				// returning an object back to the front-end
				// We will be using the mongoose method "toObject"
					// it converts to mongoose object into a plain javascript object
				return { accessToken: auth.createAccessToken(result.toObject()) }
			}else{
				// Passwords do not match
				return false;
			}
		}	

	})
}

// Start of Activity
module.exports.getProfile = (userId) =>{
	return User.findById(userId)
	.then(result => {
		if(result === null){
			return false;
		}else{
			result.password = ""
			return result;
		}
	})
}

module.exports.allUsers = ()=>{
	return User.find({})
	.then(result =>{
		return result
	})
}