const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	//Data for our fields or properties to be included when creating a record
	// The "true" value defines if the field is required or not, and the record element in the array
		// is the message that will be printed out in our terminal when the data is not present.
	name: {
		type: String,
		required: [true, "Course is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: String,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true 	// default para automatic na may na-add na course
	},
	createdOn: {
		type: Date,
		default: new Date()
		// The "new Date()" expression instantiates a new "data" that stores the current date and time
			// whenever a course is created in our database
	}
	// Concept of referencing data to establish a relationship between our courses and users (One to one relationship)
	enrollees: [
		{
			userId: {	// referencing
				type: String,
				required: [ true, "Used ID is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("Course", courseSchema);